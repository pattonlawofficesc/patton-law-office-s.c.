Patton Law, located in Kenosha Wisconsin, focuses on Criminal Defense, Juvenile Defense, Termination of Parental Rights, Child Protection (CHIPS), Indian Law/ICWA, Investigations, White Collar Crime, and Family Law and are there to help you navigate the legal system.

Address: 1119 60th St, Kenosha, WI 53140, USA

Phone: 262-221-4848

Website: https://www.pattonlawwi.com
